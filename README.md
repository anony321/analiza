# 1.txt

Przykładowy plik posiada 269748 przesłanych pakietów w zamkniętej sieci. Po porównaniu z oryginalnym plikiem można zobaczyć, że zmodyfikowano 7943 pakietów.
Zmodyfikowane pakiety posiadają zmieniony adres źródłowy (C467) oraz port docelowy (139). Zmiana imituje sytuację w której komputer C467 wysyła pakiety do losowych komputerów na porcie 139

Prawdopodobnie chodzi o wykorzystanie podatności w niezabezpieczonej usłudze SMB, która domyślnie działa na tym porcie.
Wiele ransomware wykorzystuje luki w SMB, aby umożliwić rozprzestrzenianie się na inne maszyny w sieci. 
W chwili obecnej w internecie istnieje wiele odmian tego szkodliwego oprogramowania wykorzystującego niezałatane podatności w SMB.
Publicznie znane jest kilka exploitów wykorzystujące podatności w SMB np. EternalBlue (wykorzystane w WannaCry i Emotet), EternalRomance (Bad Rabbit, NotPetya i TrickBot), EternalChampion i EternalSynergy


# 2.txt

Przykładowy plik posiada 149873 zdarzeń logowań do komputerów w sieci/serwerów (np. AD, serwisów). Po porównaniu z oryginalnym plikiem można zobaczyć, że zmodyfikowano 44843 zdarzeń logowania.
Zmodyfikowane zdarzenia symulują dużą ilość prób uwierzytelnienia (nieudanych) i czasem kończących się prawidłowym uwierzytelnieniem (nastąpiło zgadnięcie hasła).

Analiza różnic pomiędzy oryginalnym a zaatakowanym plikiem nie ma dużego sensu (dla przykładu istnieją zdarzenia wylogowania - LogOff, które się nie powiodły co nie ma sensu.. najwyraźniej ktoś nie miał pojęcia co robi)
Nie mniej jednak, zmiany w plikach powstały w następujący sposób:
1. Wybranie losowego zdarzenia
2. Zmiana ostatniej kolumny w wybranym zdarzeniu na Fail
3. Powielenie nieudanego uwierzytelnienia kilkadziesiąt razy (ok. 50~100)
4. Jeżeli nastąpiło zgadnięcie hasła to po fali nieudanych uwierzytelnień następuje prawidłowe logowanie

Przeprowadzony atak to bruteforce (słownikowy/fuzzing) mający na celu zgadnięcie hasła z wykorzystaniem słownikowej listy haseł bądź tworzenia hasła na podstawie słów kluczowych.

Dla przykładu w oryginalnym pliku ilość nieudanych logowań stanowi 0,71% wszystkich zdarzeń, po zaatakowaniu jest ich 30,41%


# 3.txt

Przykładowy plik posiada 26562 zdarzeń prawidłowego zalogowania do komputerów w sieci domenowej. Po porównaniu z oryginalnym plikiem zostało zmodyfikowanych 8871 zdarzeń.
Sposób modyfikowania zdarzeń był następujący:
1. Losowanie czasu zdarzenia (pierwsza kolumna)
2. Dla wszystkich zdarzeń posiadających wylosowany czas, aktualizujemy komputer do którego zarejestrowano prawidłowe logowanie (nadpisywanie trzeciej kolumny)

Logowania odbywały się na komputerach: C20, C216, C287 oraz C524

W pliku znajdują się jedynie prawidłowe logowania do komputera (nigdzie nie jest powiedziane, czy są to logowania lokalne/zdalne)
Obstawiam, że doszło tutaj do przejęcia podanych wyżej komputerów przez atakujących (bądź uzyskana możliwość zdalnego logowania) i próba złamania haseł użytkowników domenowych z wykorzystaniem tychże maszyn.

Dla potrzeb zadania zakładam, że nie ma skonfigurowanego limitu na ilość nieudanych logowań w AD (Active Directory) bądź jest on na tyle wysoki, że pozwala na dużą liczbę prób przed zablokowaniem komputera.

Atak wygląda następująco:
1. Atakujący uzyskują dostęp do komputera w sieci
2. Za pomocą AD uzyskuje informacje o innych użytkownikach w domenie (bądź z innych źródeł)
3. Rozpoczęcie próby zgadnięcia haseł znalezionych użytkowników za pomocą ataku bruteforce z wykorzystaniem słowników

(przykładowy kod w python-ie symulujący powyższy zachowanie)
```python
import ldap # biblioteka umożliwiająca logowanie do domeny

logins = [...] # lista znalezionych użytkowników
passwords = [...] # lista często używanych haseł do ataku słownikowego

conn = ldap.open("domain..")
for password in passwords:
	for login in logins:
		try:
			conn.simple_bind_s(login + "@domain..", password)
			print("Success.. found user password!", login, password)
		except ldap.INVALID_CREDENTIALS:
			pass
```


Pojawienie się wielu zdarzeń w tym samym czasie o prawidłowym logowaniu przez wielu użytkowników prawdopodobnie świadczy o tym, że używają tego samego hasła.
